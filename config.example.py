PORT = 8069

PASSWORD = '123456'

SLOW_THRESHOLD = 2000

SERVICES = {
    'elixire': {
        'description': "elixi.re's backend",
        'adapter': 'http',
        'adapter_args': {
            'url': 'https://elixi.re/api/hello'
        },
        'poll': 10,
        'alerts': ['beep'],
    },
    'dabian': {
        'description': 'elixi.re main server',
        'adapter': 'ping',
        'adapter_args': {
            'address': '192.168.1.1'
        },
        'poll': 15,
        'alerts': ['beep'],
    }
}

ALERTS = {
    'bepsi': {
        'type': 'discord',
        'url': 'beep boop'
    }
}

INCIDENT_ALERT = 'bepsi'
