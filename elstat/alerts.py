import logging

from aiohttp import ClientSession

from elstat.blueprints.incidents import to_isots

log = logging.getLogger(__name__)


def _make_payload(worker, status: dict):
    service = worker.service

    serv_name = service['name']
    is_up = status['status']

    err = status.get('error', 'No error provided')
    color = 0x00ff00 if is_up else 0xff0000

    embed = {
        'title': serv_name,
        'color': color,
        'timestamp': to_isots(status['timestamp'] / 1000)
    }

    if not is_up:
        embed['description'] = f'error: {err}'
    else:
        # calculate downtime here

        # first we need to find the work that had a success
        # before the current one
        conn = worker.manager.conn
        cur = conn.cursor()

        # find the last work that had a success
        cur.execute(f"""
        SELECT timestamp FROM {worker.name}
        WHERE timestamp < ? AND status = 1
        ORDER BY timestamp DESC
        LIMIT 1
        """, (status['timestamp'], ))

        row = cur.fetchone()
        ts_success = row[0]

        # now we fetch all the downtime after that ts_success
        cur.execute(f"""
        SELECT COUNT(*) FROM {worker.name}
        WHERE timestamp > ? AND status = 0
        """, (ts_success,))

        row = cur.fetchone()
        count = row[0]

        downtime_msec = count * worker.service['poll'] * 1000
        downtime_sec = downtime_msec / 1000
        downtime_min = round(downtime_sec / 60, 3)

        embed['footer'] = {
            'text': f'down for {downtime_min} minutes '
                    f'({downtime_sec} seconds)'
        }

    return {
        'content': '',
        'embeds': [embed],
    }


def _make_payload_latency(worker, status):
    service = worker.service

    latency = status['latency']
    max_latency = worker.manager.cfg.SLOW_THRESHOLD

    color = 0xff7f50 if latency > max_latency else 0x00ff00

    embed = {
        'title': service['name'],
        'color': color,
        'timestamp': to_isots(status['timestamp'] / 1000),
        'description': f'current latency: {latency}ms'
    }

    return {
        'content': '',
        'embeds': [embed]
    }


class DiscordAlert:
    """Represents a Discord Webhook."""
    def __init__(self, alert_name: str, alert: dict):
        self.name = alert_name
        self.url = alert['url']
        self.session = ClientSession()

    async def post(self, worker, status: dict):
        """Post to an alert given the service and its status."""
        payload = _make_payload(worker, status)
        return await self.post_raw(payload)

    async def post_latency(self, worker, status: dict):
        """Post to an alert about latency being too big."""
        payload = _make_payload_latency(worker, status)
        return await self.post_raw(payload)

    async def _post(self, url: str, payload: dict):
        log.debug('payload: %r', payload)

        async with self.session.post(url, json=payload) as resp:
            log.debug('webhook post status: %d', resp.status)
            return resp

    async def post_raw(self, payload: dict):
        """Post a raw payload to the alert."""

        # check multi-webhook alerts, only return the last resp.
        if isinstance(self.url, list):
            resp = None

            for url in self.url:
                resp = await self._post(url, payload)

            return resp

        return await self._post(self.url, payload)
