import hashlib

from .errors import Unauthorized


def auth_route(handler):
    """Declare an authenticated route."""
    async def _handler(request, *args, **kwargs):
        try:
            pwhash = request.headers['Authorization']
        except KeyError:
            raise Unauthorized('no password provided')

        correct = request.app.cfg.PASSWORD
        hashed = hashlib.sha256(correct.encode()).hexdigest()

        if pwhash != hashed:
            raise Unauthorized('invalid password')

        return await handler(request, *args, **kwargs)

    return _handler
