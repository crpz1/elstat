from .adapters import HttpAdapter, PingAdapter
from .alerts import DiscordAlert


ADAPTERS = {
    'http': HttpAdapter,
    'ping': PingAdapter,
}

ALERTS = {
    'discord': DiscordAlert
}


class IncidentType:
    OUTAGE = 'outage'
    PARTIAL_OUTAGE = 'partial_outage'
    DEGRADED = 'degraded_service'
