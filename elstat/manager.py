import asyncio
import logging
import json

from typing import List, Any

from .consts import ADAPTERS, ALERTS
from .worker import ServiceWorker

from .blueprints.streaming import OP
from .blueprints.incidents import IncidentAlert, to_isots

log = logging.getLogger(__name__)

_COLUMNS = {
    'timestamp': 'timestamp bigint',
    'status': 'status bool',
    'latency': 'latency bigint',
}


class ServiceManager:
    """Service manager for elstat."""
    def __init__(self, app):
        self.app = app
        self.conn = app.conn

        self.workers = {}
        self.alerts = {}
        self.state = {}
        self.subscribers = {}
        self._websockets = {}

        self._start()

    @property
    def cfg(self):
        """Return the instance's configuration."""
        return self.app.cfg

    @property
    def loop(self):
        """Return the asyncio event loop."""
        return self.app.loop

    def _make_db_table(self, name: str, service: dict):
        """Create tables in elstat's database for services and incidents."""
        adapter = ADAPTERS[service['adapter']]

        columnstr = map(_COLUMNS.get, adapter.spec['db'])
        columnstr = ',\n'.join(columnstr)

        log.info(f'Making table for {name}')
        self.conn.executescript(f"""
        CREATE TABLE IF NOT EXISTS {name} (
        {columnstr}
        );
        """)

        self.conn.executescript("""
        CREATE TABLE IF NOT EXISTS incidents (
            id bigint PRIMARY KEY,
            incident_type text,
            title text,
            content text,
            ongoing bool,
            start_timestamp bigint,
            end_timestamp bigint
        );

        CREATE TABLE IF NOT EXISTS incident_stages (
            parent_id bigint REFERENCES incidents (id) NOT NULL,
            timestamp bigint,
            title text,
            content text,
            PRIMARY KEY (parent_id, timestamp)
        );
        """)

    def _check(self, columns: tuple, field: str, worker_name: str):
        """Create channels based on the adapter's columns."""
        chan_name = f'{field}:{worker_name}'

        if field in columns and chan_name not in self.subscribers:
            self.subscribers[chan_name] = []
            log.info(f'Created channel {chan_name}')

    def _create_channels(self, worker):
        """Create streaming channels for a worker"""
        columns = worker.adapter.spec['db']

        # each service has a status and latency channel
        self._check(columns, 'status', worker.name)
        self._check(columns, 'latency', worker.name)

    def _start(self):
        self.subscribers['incidents'] = []

        # init services
        for name, service in self.cfg.SERVICES.items():
            self._make_db_table(name, service)
            service['name'] = name

            # spawn a service worker
            serv_worker = ServiceWorker(self, name, service)
            self.workers[name] = serv_worker
            self.state[name] = None

            self._create_channels(serv_worker)

        # init alerts
        for name, alert in self.cfg.ALERTS.items():
            alert_cls = ALERTS[alert['type']]
            self.alerts[name] = alert_cls(name, alert)

    async def close(self):
        """Close the server."""
        log.info('closing manager')

        for worker in self.workers.values():
            worker.stop()

        log.info('closing %d websockets', len(self._websockets))
        for conn in self._websockets.values():
            asyncio.ensure_future(
                conn.close(1001, reason='Server is shutting down')
            )

        log.info('closing %d sessions in alerts', len(self.alerts))
        for alert_obj in self.alerts.values():
            await alert_obj.session.close()

    def subscribe(self, channels: List[str], websocket) -> List[str]:
        """Subscribe a websocket to given channels."""

        wid = websocket.client_id
        subscribed = []

        self._websockets[websocket.client_id] = websocket

        for chan in channels:
            try:
                self.subscribers[chan].append(wid)
                subscribed.append(chan)
            except KeyError:
                pass

        log.info(f'Subscribed {wid} to {subscribed}')
        return subscribed

    def unsubscribe(self, channels: List[str], websocket) -> List[str]:
        """Unsubscribe a websocket from given channels."""
        wid = websocket.client_id
        unsub = []

        for chan in channels:
            try:
                self.subscribers[chan].remove(wid)
                unsub.append(chan)
                log.info(f'Unsubscribed {wid} from {chan}')
            except (KeyError, ValueError):
                pass

        return unsub

    def unsub_all(self, websocket):
        """Unsubscribe a websocket from all channels its subscribed to."""
        unsub = []

        for chan, subs in self.subscribers.items():
            try:
                subs.remove(websocket.client_id)
                unsub.append(chan)
            except ValueError:
                pass

        log.info(f'unsubscribed {websocket.client_id} from {unsub}')

        try:
            self._websockets.pop(websocket.client_id)
        except KeyError:
            pass

        return unsub

    def _ws_send(self, websocket, data: Any):
        """Send data to a websocket."""
        if websocket is None:
            return None

        loop = self.app.loop
        data = json.dumps(data)

        return loop.create_task(websocket.send(data))

    def _raw_send(self, websocket, channel: str, data: Any):
        return self._ws_send(websocket, {
            'op': OP.DATA,
            'c': channel,
            'd': data,
        })

    def publish(self, channel: str, data: Any):
        """Publish data to a channel."""

        # get a list of websocket clients based on the websocket
        # ids in the channel
        ws_ids = self.subscribers[channel]
        websockets = map(self._websockets.get, ws_ids)

        def _send(websocket):
            return self._raw_send(websocket, channel, data)

        # create a task for each client instead of
        # dispatching to each one in a loop (which could become a bottleneck)
        tasks = list(map(_send, websockets))

        clients = len(tasks)
        if clients:
            log.info(f'Dispatching on {channel} to {clients} clients')

        return tasks

    def publish_incident(self, op_code: int, data: Any):
        """Publish an incident in the 'incidents' channel."""
        ws_ids = self.subscribers['incidents']
        websockets = map(self._websockets.get, ws_ids)

        def _send(websocket):
            return self._ws_send(websocket, {
                'op': op_code,
                'c': 'incidents',
                'd': data,
            })

        tasks = map(_send, websockets)
        return list(tasks)

    def _get_partial_incident(self, incident_id, fields: str) -> tuple:
        """Get partial incident information, given the fields."""
        cur = self.conn.cursor()

        cur.execute(f"""
        SELECT {fields}
        FROM incidents
        WHERE id = ?
        """, (incident_id,))

        return cur.fetchone()

    def _get_stage(self, incident_id, timestamp) -> tuple:
        """Get an incident stage."""
        cur = self.conn.cursor()

        cur.execute("""
        SELECT title, content
        FROM incident_stages
        WHERE parent_id = ? AND timestamp = ?
        """, (incident_id, timestamp))

        return cur.fetchone()

    async def _alert_new_incident(self, incident_id):
        row = self._get_partial_incident(
            incident_id,
            'incident_type, title, content, start_timestamp'
        )

        embed = {
            'title': f'New Incident - {row[0]} - {row[1]}',
            'description': row[2],
            'timestamp': to_isots(row[3]),
            'color': 0xff0000,
            'footer': {
                'text': f'incident id: {incident_id}',
            },
        }

        return embed

    async def _alert_close_incident(self, incident_id):
        row = self._get_partial_incident(
            incident_id,
            'title, end_timestamp'
        )

        embed = {
            'title': f'Closed incident - {row[0]}',
            'timestamp': to_isots(row[1]),
            'color': 0x00ff00,
            'footer': {
                'text': f'incident id: {incident_id}',
            },
        }

        return embed

    async def _alert_new_stage(self, incident_id, timestamp):
        incident = self._get_partial_incident(
            incident_id,
            'title'
        )

        stage = self._get_stage(incident_id, timestamp)

        embed = {
            'title': f'Stage for {incident[0]!r} - {stage[0]}',
            'description': stage[1],
            'timestamp': to_isots(timestamp),
            'color': 0xffff00,
            'footer': {
                'text': f'incident id: {incident_id}',
            },
        }

        return embed

    async def alert_incident(self, alert_type, incident_id, *args, **kwargs):
        """Send an alert related to an incident.

        This generates embeds specific to DiscordAlert.
        """
        handler = {
            IncidentAlert.new_incident: self._alert_new_incident,
            IncidentAlert.close_incident: self._alert_close_incident,
            IncidentAlert.new_stage: self._alert_new_stage
        }[alert_type]

        embed = await handler(incident_id, *args, **kwargs)

        alert_key = self.cfg.INCIDENT_ALERT

        try:
            alert = self.alerts[alert_key]
            await alert.post_raw(embed)
        except KeyError:
            log.warning('no alert setup for incidents')
            return
