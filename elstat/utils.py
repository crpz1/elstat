def fetchall(conn, query, *args) -> list:
    """Fetch all rows from a query."""
    cur = conn.cursor()
    cur.execute(query, *args)
    res = cur.fetchall()
    cur.close()
    return res
