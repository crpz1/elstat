import time
import asyncio
import logging

from .consts import ADAPTERS
from elstat.utils import fetchall


def do_latency(lat1, lat2, lat3, threshold) -> bool:
    """Tell if we should do any latency alerts.

    lat1 must be the earliest datapoint, and lat3 the oldest.
    """
    lat1 = lat1 > threshold
    lat2 = lat2 > threshold
    lat3 = lat3 > threshold

    # the expression is L1 != (L2 and L3)
    # the (L2 and L3) part cleans us up from peaks that happened between
    # L2 and L3.

    # but ignoring the case where L1=1,L2=0,L3=0, since L1=1
    # could very much be a peak datapoint we don't want to
    # work with.

    lat_flag = lat2 and lat3

    if lat1 and not lat2 and not lat3:
        return False

    return lat1 != lat_flag


class ServiceWorker:
    """Represents a worker for a service.

    This is the base generalistic class that uses
    an Adapter.

    Attributes
    ----------
    name: str
        string of the service being checked
    manager: :class:`ServiceManager`
        pointer to the main manager class.
    service: dict
        service information
    adapter: :class:`Adapter`
        service adapter
    log: :inst:`logging.Logger`
        logger for that specific worker
    last_poll: float
        time where last adapter poll happened (monotonic)
    """
    def __init__(self, manager, name, service):
        self.name = name
        self.manager = manager
        self.service = service
        self.adapter = ADAPTERS[service['adapter']]
        self.log = logging.getLogger(f'elstat.service.{name}')
        self.last_poll = None

        self._start()

    async def work(self) -> dict:
        """Call the adapter"""
        return await self.adapter.query(self, self.service['adapter_args'])

    async def process_work(self, result: dict):
        """Process given adapter result and insert into
        the database.

        Dispatches the given work to streaming clients
        and checks if any alerts need to be done.
        """
        try:
            # we work with a copy of result for main db
            # operations (without error field)
            db_res = dict(result)
            db_res.pop('error')
        except KeyError:
            pass

        columns = self.adapter.spec['db']
        conn = self.manager.conn

        timestamp = int(time.time() * 1000)

        args_str = ','.join(['?'] * (len(db_res) + 1))
        query = f"""
        INSERT INTO {self.name} ({','.join(columns)})
        VALUES ({args_str})
        """

        args = []
        for col in columns[1:]:
            val = db_res[col]
            args.append(val)

        self.log.debug('status: %r, latency: %d', args[0], args[1])

        conn.execute(query, (timestamp, ) + tuple(args))
        conn.commit()

        await self._dispatch_work(columns, timestamp, result)

        result['timestamp'] = timestamp
        await self._check_alert(result)

    async def _dispatch_work(self, columns, timestamp: int, result: tuple):
        """Dispatch the work done by the adapter
        through the channels"""
        prechan = columns[1:]
        chans = [f'{chan}:{self.name}' for chan in prechan]

        for col, chan in zip(prechan, chans):
            self.manager.publish(chan, [timestamp, result[col]])

    async def _check_alert(self, work):
        """Check if any alerts should be thrown off by status changes."""
        rows = fetchall(self.manager.conn, f"""
        SELECT status, latency, timestamp
        FROM {self.name}
        ORDER BY timestamp DESC
        LIMIT 3
        """)

        # if we don't have the rows the db is probably being initalized
        # so we can't really assume stuff will work.
        if len(rows) < 3:
            return

        # extract rows
        last, first_1, first_0 = rows

        # status checking only works with the last 2 rows
        first_status, last_status = first_0[0], last[0]
        threshold = self.manager.cfg.SLOW_THRESHOLD

        # latency checking occours with the last 3 rows
        should_latency = do_latency(
            first_0[1], first_1[1], last[1], threshold)

        self.log.debug('l1:%r l2:%r l3:%r, alert? %r',
                       first_0[1], first_1[1], last[1], should_latency)

        self.log.debug('f0:s %r, l:s %r',
                       first_status, last_status)

        should_status = first_status != last_status

        # if they're the same, there isn't a need to alert
        if not should_status and not should_latency:
            return

        # oopsie whoopsie time to alertie owo
        alerts = self.service.get('alerts', [])

        if not alerts:
            self.log.warning(f'no alerts set for {self.name}')
            return

        for alert in alerts:
            alert_obj = self.manager.alerts[alert]

            if should_latency:
                await alert_obj.post_latency(self, work)
            elif should_status:
                await alert_obj.post(self, work)

    async def _work_loop(self):
        try:
            while True:
                self.last_poll = time.monotonic()
                res = await self.work()

                self.manager.state[self.name] = res
                await self.process_work(res)

                await asyncio.sleep(self.service['poll'])
        except asyncio.CancelledError:
            self.log.info('cancelled, stopping')
        except Exception as err:
            self.log.exception('fail on work loop, retrying')
            try:
                # hardcode a bad result on workloop failures
                result = {
                    'status': False,
                    'latency': 0,
                    'error': str(err),
                }

                # FORCE EVERYONE TO KNOW ABOUT THAT FAILURE
                self.manager.state[self.name] = result
                await self.process_work(result)
            except KeyError:
                pass

            # give a hard 1 second cooldown before restarting
            # the worker
            await asyncio.sleep(1)
            await self._work_loop()

    def stop(self):
        """Stop the current worker."""
        self._work_task.cancel()

    def _start(self):
        self.log.info(f'starting work loop for {self.name}')
        self._work_task = self.manager.loop.create_task(self._work_loop())
