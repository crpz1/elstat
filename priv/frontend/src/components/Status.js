import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import './Status.css'
import Incident from './Incident'

export default function Status({ incident }) {
  const incidentOngoing = incident && incident.ongoing === 1
  const view = incidentOngoing ? (
    <Incident incident={incident} />
  ) : (
    'All systems operational'
  )

  const className = classnames(
    'status',
    incidentOngoing ? 'status-bad' : 'status-good'
  )
  return <div className={className}>{view}</div>
}

Status.propTypes = {
  incident: PropTypes.object,
}
