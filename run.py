import logging
import sqlite3
from os.path import isdir

import aiohttp
from sanic import Sanic, response
from sanic_cors import CORS
from sanic.exceptions import NotFound, FileNotFound

import config

from elstat.manager import ServiceManager
from elstat.blueprints import api, streaming, incidents
from elstat.blueprints.errors import ApiError

log = logging.getLogger(__name__)

if getattr(config, 'DEBUG', False):
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

app = Sanic()
app.cfg = config
CORS(app, automatic_options=True)

app.blueprint(api)
app.blueprint(incidents)
app.blueprint(streaming)


@app.listener('before_server_start')
async def _app_start(refapp, loop):
    refapp.session = aiohttp.ClientSession(loop=loop)
    refapp.conn = sqlite3.connect('elstat.db')
    refapp.manager = ServiceManager(app)


@app.listener('before_server_stop')
async def _app_stop(refapp, _loop):
    await refapp.manager.close()
    refapp.conn.close()


@app.exception(ApiError)
async def _handle_api_err(request, exception):
    return response.json({
        'error': True,
        'code': exception.status_code,
        'message': exception.message
    }, status=exception.status_code)


@app.exception(Exception)
async def _handle_exc(request, exception):
    status_code = 404 if isinstance(exception, (NotFound, FileNotFound)) \
        else 500

    if status_code != 404:
        log.exception('oopsie woopsie')

    return response.json({
        'error': True,
        'code': status_code,
        'message': repr(exception)
    }, status=status_code)


if __name__ == '__main__':
    if isdir('./priv/frontend/build'):
        app.static('/', './priv/frontend/build')
        app.static('/', './priv/frontend/build/index.html')

    app.run(port=config.PORT, host='0.0.0.0')
